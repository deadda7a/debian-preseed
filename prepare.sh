#!/usr/bin/env bash
# Sebastian Elisa Pfeifer <sebastian@sebastian-elisa-pfeifer.eu>

targets=(cli gui)

for target in "${targets[@]}"; do
  cp shared/downloads profiles/${target}.downloads
  cp shared/excludes profiles/${target}.excludes
  cp shared/packages profiles/${target}.packages
  cp shared/udebs profiles/${target}.udebs
  cat shared/preseed >> profiles/${target}
  mv profiles/${target} profiles/${target}.preseed
done
