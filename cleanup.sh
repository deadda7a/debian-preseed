#!/usr/bin/env bash
# Sebastian Elisa Pfeifer <sebastian@sebastian-elisa-pfeifer.eu>

git checkout -f
targets=(cli gui)

for target in "${targets[@]}"; do
  rm profiles/${target}.downloads
  rm profiles/${target}.excludes
  rm profiles/${target}.packages
  rm profiles/${target}.udebs
  rm profiles/${target}.preseed
done

rm -rf tmp
rm -rf images
